This repository contains some parts of the ROS packages developed by the [Sarbot Team](http://www.sarbot-team.es/) for the [Virtual Robotics Challenge (VRC)](http://www.theroboticschallenge.org/participate.aspx).

# Documentation

* See the installation instructions below.
* This page.
* Throughout the various files in the package.
* For questions, please use [http://answers.ros.org](http://answers.ros.org) or [http://answers.gazebosim.org](http://answers.gazebosim.org) depending on the topic.

# Installation

Go to your ROS working directory. eg.
```
cd ~/ros
```

Clone the `atlas-arm-manipulation` stack from bitbucket:

```
$ git clone https://bitbucket.org/fsuarez6/atlas-arm-manipulation.git
```

The `atlas_arm_kinematics` package depends on the `arm_kinematics_tools` package ([wiki](http://www.ros.org/wiki/arm_kinematics_tools)). 
Please install it from the svn repository:

```
$ svn checkout http://kaist-ros-pkg.googlecode.com/svn/trunk/arm_kinematics_tools
$ rosmake arm_kinematics_tools
```

After installing the `arm_kinematics_tools` package you should be able to build the `atlas-arm-manipulation` stack:
```
$ rosmake atlas-arm-manipulation
```
If you have missing dependencies please install them using the official repositories.

# Software Versions

**Gazebo** 1.7.3 or above

**DRCSim** 2.5.2 or above

**Sandia Hands** 5.1.13 or above

**OSRF Common** 1.0.6 or above

# Usage

## Qualification - Task 2: Manipulate

Task Qual-2 (short for VRC Qualification Task Two) shall be to pick up a cordless drill from a table and place it in a bin next to the table.

Start the qual_task_2 world:
```
$ roslaunch atlas_utils qual_task_2.launch 
```
Launch the kinematic controller:
```
$ roslaunch atlas_control qual_task_2.launch 
```
