#!/usr/bin/env python
#
# Copyright 2013 Open Source Robotics Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Desc: helper script for spawning models in gazebo
#
# Author: Francisco Suárez Ruiz
#

import roslib; roslib.load_manifest('atlas_control')
import rospy, yaml, sys
from ArmControl import ArmControl
from TorsoControl import TorsoControl


def parse_yaml():
    # first make sure the input arguments are correct
    if len(sys.argv) not in [3,5]:
        print 'usage: qual_task_2.py YAML_FILE TRAJECTORY_NAME'
        print '    where TRAJECTORY is list of dictionaries defined in YAML_FILE'
        sys.exit(1)
    traj_yaml = yaml.load(file(sys.argv[1], 'r'))
    traj_name = sys.argv[2]
    if not traj_name in traj_yaml:
        print 'unable to find trajectory %s in %s' % (traj_name, sys.argv[1])
        sys.exit(1)
    str_list = traj_yaml[traj_name]
    data = []
    keys = []
    for index in range(len(str_list)):
        keys.append(str_list[index][0])
        data.append([ float(x) for x in str_list[index][1].split() ])
    return data, keys

def main():
    # Parse the .yaml file
    actions_data, actions_keys = parse_yaml()
    # Starts the right arm controller
    right_arm = ArmControl('right_arm')
    # Starts the torso controller
    torso = TorsoControl()
    # Set back_lbz = 0.8
    torso.moveToJointGoal(7.5, 0.8)    
    
    # Validate dictionary type: Pose Goal or Joint Goal
    for index in range(len(actions_keys)):
        data = actions_data[index]
        data_len = len(data)
        if actions_keys[index] == 'pose':
            rospy.loginfo('Moving to pose goal...')
            # Validates the position vector length
            if data_len == 8:
                dt = data[0]
                position = data[1:4]
                orientation = data[4::]
                right_arm.moveToPoseGoal(dt, position, orientation)
            else:
                print 'ERROR: Action ' + actions_keys[index] + ' badly defined'
                return
        elif actions_keys[index] == 'joint':  # Joint Goal
            rospy.loginfo('Moving to joint goal...')
            # Validates the position vector length
            if data_len == 7:
                dt = data[0]
                position = data[1::]
                right_arm.moveToJointGoal(dt, position)
            else:
                print 'ERROR: Action ' + actions_keys[index] + 'badly defined'
                return
        elif actions_keys[index] == 'grasp':
            rospy.loginfo('Performing grasp action...')
            # Validate the position vector length
            if data_len == 3:
                dt = data[0]
                grasp = int(data[1] - 1)
                amount = data[2]
                right_arm.simpleGrasp(dt, grasp, amount) 
            else:
                print 'ERROR: Action ' + actions_keys[index] + 'badly defined'
                return
        else:
            print 'ERROR: Action ' + actions_keys[index] + 'not defined' 
            return

if __name__ == '__main__':
    rospy.init_node('move_arm_qual_2')
    main()
