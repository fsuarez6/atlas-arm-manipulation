#!/usr/bin/env python
#
# Copyright 2013 Open Source Robotics Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Desc: helper script for spawning models in gazebo
#
# Author: Francisco Suárez Ruiz
#

import rospy
from math import ceil 
from numpy import array, linspace  
from atlas_msgs.msg import AtlasState
from sensor_msgs.msg import JointState
from osrf_msgs.msg import JointCommands

class TorsoControl:   
    def __init__(self):
        self._NUM_JOINTS = 3
        # Latest message from atlas/joint_states
        self._jointState_msg = JointState()
        # Set up publishers / subscribers
        rospy.Subscriber('atlas/joint_states', JointState, self.__jointState_cb)
        self._pub_user_mode = rospy.Publisher('atlas/torso_control', JointCommands)
        # Wait for the publishers
        rospy.sleep(2.0)
    
    def __jointState_cb(self, msg):
        self._jointState_msg = msg
        self._joint_names = ['back_lbz', 'back_mby', 'back_ubx']
        self._current_joint_position = list(msg.position[AtlasState.back_lbz:AtlasState.back_ubx+1])
        # print '[DEBUG] Current joint position TORSO: ' + str(self._current_joint_position)
    
    # TODO: Calculate the goal time from the joints limits
    def moveToJointGoal(self, dt, back_lbz, back_mby = None, back_ubx = None):
        # Creates the command message that will be sent to 'atlas/torso_control'
        command = JointCommands()
        command.name = self._joint_names
        command.header.stamp = rospy.Time.now()
        # Current joint position
        initial_angles = array(self._current_joint_position)
        # Validate the given arguments 
        if (back_mby == None) and (back_ubx == None): 
            goal_angles = [back_lbz, 0, 0]
            command.effort = [255, 0, 0]
        elif (back_ubx == None):
            goal_angles = [back_lbz, back_mby, 0]
            command.effort = [255, 255, 0]
        else:
            goal_angles = [back_lbz, back_mby, back_ubx]
            command.effort = [255] * 3
        commandPosition = array(goal_angles)
        goal_time = dt
        dtPublish = 0.02
        n = ceil((goal_time) / dtPublish)
        print '[TORSO] Moving torso'
        for ratio in linspace(0, 1, n):
            interpCommand = (1-ratio)*initial_angles + ratio * commandPosition
            command.position = [ float(x) for x in interpCommand ]
            self._pub_user_mode.publish(command)
            rospy.sleep(goal_time / float(n))
