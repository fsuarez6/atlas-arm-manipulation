#!/usr/bin/env python
#
# Copyright 2013 Open Source Robotics Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Desc: helper script for spawning models in gazebo
#
# Author: Francisco Suárez Ruiz
#

import roslib; roslib.load_manifest('atlas_control')
import rospy
from sensor_msgs.msg import JointState
from osrf_msgs.msg import JointCommands
from atlas_msgs.msg import AtlasSimInterfaceCommand, AtlasCommand, AtlasState, AtlasSimInterfaceState

class ControlSwitch:
    # Pseudo-constants
    STAND = AtlasSimInterfaceCommand.STAND
    USER = AtlasSimInterfaceCommand.USER
    FREEZE = AtlasSimInterfaceCommand.FREEZE
    STAND_PREP = AtlasSimInterfaceCommand.STAND_PREP
    WALK = AtlasSimInterfaceCommand.WALK
    STEP = AtlasSimInterfaceCommand.STEP
    MANIPULATE = AtlasSimInterfaceCommand.MANIPULATE
    NUM_JOINTS = 28
    JOINT_NAMES = ('back_lbz',  'back_mby', 'back_ubx', 'neck_ay', 'l_leg_uhz', 'l_leg_mhx', 'l_leg_lhy', 
                   'l_leg_kny', 'l_leg_uay','l_leg_lax','r_leg_uhz', 'r_leg_mhx', 'r_leg_lhy','r_leg_kny',
                   'r_leg_uay', 'r_leg_lax','l_arm_usy', 'l_arm_shx', 'l_arm_ely','l_arm_elx', 'l_arm_uwy',
                   'l_arm_mwx','r_arm_usy', 'r_arm_shx', 'r_arm_ely', 'r_arm_elx', 'r_arm_uwy', 'r_arm_mwx') 
    
    def __init__(self):
        n = self.NUM_JOINTS
        # Prepares the user msg
        self.user_msg = AtlasCommand()
        self.user_msg.position     = [0.0] * n
        self.user_msg.k_effort     = [0.0] * n
        self.user_msg.velocity     = [0.0] * n
        self.user_msg.kp_position  = [0.0] * n
        self.user_msg.ki_position  = [0.0] * n
        self.user_msg.kd_position  = [0.0] * n
        self.user_msg.kp_velocity  = [1.0] * n      # Default one for the amrs
        self.user_msg.i_effort_min = [0.0] * n
        self.user_msg.i_effort_max = [0.0] * n

        for i in range(self.NUM_JOINTS):
            name = self.JOINT_NAMES[i]
            self.user_msg.kp_position[i]  = rospy.get_param('atlas_controller/gains/' + name + '/p')
            self.user_msg.ki_position[i]  = rospy.get_param('atlas_controller/gains/' + name + '/i')
            self.user_msg.kd_position[i]  = rospy.get_param('atlas_controller/gains/' + name + '/d')
            self.user_msg.i_effort_max[i] = rospy.get_param('atlas_controller/gains/' + name + '/i_clamp')
            if rospy.has_param('atlas_controller/gains/' + name + '/kp_vel'):
                self.user_msg.kp_velocity[i] = rospy.get_param('atlas_controller/gains/' + name + '/kp_vel')
            self.user_msg.i_effort_min[i] = -self.user_msg.i_effort_max[i]
        
        # Prepares the BDI msg
        self.bdi_msg = AtlasSimInterfaceCommand()
        
        # Small hack so PyDev knows the object type for these variables
        self.joint_states = JointState()
        self.sim_states = AtlasSimInterfaceState()
        # Wait for states to hook up
        rospy.loginfo('Waiting for atlas state topics')
        self.joint_states = None
        self.sim_states = None
            
        # Set up publishers / subscribers
        self.pub_ac = rospy.Publisher('atlas/atlas_command', AtlasCommand)
        self.pub_asic = rospy.Publisher('atlas/atlas_sim_interface_command', AtlasSimInterfaceCommand)
        rospy.Subscriber('atlas/joint_states', JointState, self.joint_state_cb)
        rospy.Subscriber('atlas/atlas_sim_interface_state', AtlasSimInterfaceState, self.sim_state_cb)
        
        # Custom topics for sections control (arms, torso)
        rospy.Subscriber('atlas/right_arm_control', JointCommands, self.right_arm_cb)
        rospy.Subscriber('atlas/left_arm_control', JointCommands, self.left_arm_cb)
        rospy.Subscriber('atlas/torso_control', JointCommands, self.torso_cb)
        
        while (self.joint_states == None) or (self.sim_states == None):
            rospy.sleep(0.001) 
        self.state = self.sim_states.current_behavior;
        self.user_msg.position = list(self.joint_states.position)
        
        # Debug
        rospy.logdebug('kp_position: ' + str(self.user_msg.kp_position))    
        rospy.logdebug('kd_position: ' + str(self.user_msg.kd_position))
        rospy.logdebug('ki_position: ' + str(self.user_msg.ki_position))
        
    def sim_state_cb(self, msg):
        self.sim_states = msg
        
    def joint_state_cb(self, msg):
        self.joint_states = msg
        # Wait for the sim states 
        if self.sim_states == None:
            return
        # Reads the initial joints from the STAND state (previous state)
        if self.sim_states.current_behavior == self.STAND:
            self.user_msg.position = list(self.joint_states.position)
        
    def right_arm_cb(self, msg):
        self.state = self.MANIPULATE  
        msg_size = len(msg.position)
        expected_size = 6
        if msg_size != expected_size:
            rospy.logwarn('sarbot/right_arm_control, wrong message size: %s (expected: %s)' % (msg_size, expected_size))
            return
        self.user_msg.position[AtlasState.r_arm_usy:AtlasState.r_arm_mwx+1] = msg.position
        # TODO: Improve this assignment
        self.user_msg.k_effort[AtlasState.r_arm_usy:AtlasState.r_arm_mwx+1] = [255] * 6
        self.user_msg.k_effort[AtlasState.l_arm_usy:AtlasState.l_arm_mwx+1] = [255] * 6
        self.user_msg.k_effort[AtlasState.back_lbz:AtlasState.back_ubx+1] = [255] * 3
        self.user_msg.k_effort[AtlasState.neck_ay] = 255
        
    def left_arm_cb(self, msg):
        self.state = self.MANIPULATE
        msg_size = len(msg.position)
        expected_size = 6
        if msg_size != expected_size:
            rospy.logwarn('sarbot/left_arm_control, wrong message size: %s (expected: %s)' % (msg_size, expected_size))
            return
        self.user_msg.position[AtlasState.l_arm_usy:AtlasState.l_arm_mwx+1] = msg.position  
        # TODO: Improve this assignment
        self.user_msg.k_effort[AtlasState.r_arm_usy:AtlasState.r_arm_mwx+1] = [255] * 6
        self.user_msg.k_effort[AtlasState.l_arm_usy:AtlasState.l_arm_mwx+1] = [255] * 6
        self.user_msg.k_effort[AtlasState.back_lbz:AtlasState.back_ubx+1] = [255] * 3
        self.user_msg.k_effort[AtlasState.neck_ay] = 255
        
    def torso_cb(self, msg):
        self.state = self.MANIPULATE
        msg_size = len(msg.position)
        expected_size = 3
        if msg_size != expected_size:
            rospy.logwarn('sarbot/torso_control, wrong message size: %s (expected: %s)' % (msg_size, expected_size))
            return 
        self.user_msg.position[AtlasState.back_lbz:AtlasState.back_ubx+1] = msg.position
        # TODO: Improve this assignment
        self.user_msg.k_effort[AtlasState.r_arm_usy:AtlasState.r_arm_mwx+1] = [255] * 6
        self.user_msg.k_effort[AtlasState.l_arm_usy:AtlasState.l_arm_mwx+1] = [255] * 6
        self.user_msg.k_effort[AtlasState.back_lbz:AtlasState.back_ubx+1] = [255] * 3
        self.user_msg.k_effort[AtlasState.neck_ay] = 255
    
    def update(self):
        rospy.logdebug('current_behavior: ' + str(self.sim_states.current_behavior))
        if self.state != self.MANIPULATE:
            return
        
        # Always insert current time
        self.bdi_msg.header.stamp = rospy.Time.now()
        self.user_msg.header.stamp = rospy.Time.now()
        
        self.pub_ac.publish(self.user_msg)
        
        current_behavior = self.sim_states.current_behavior 
        if current_behavior == self.STAND:
            # This part is in case they start to use the manipulation parameters
            self.bdi_msg.manipulate_params.use_desired = True
            self.bdi_msg.manipulate_params.desired.pelvis_height = 0.9
            self.bdi_msg.manipulate_params.desired.pelvis_lat = 0.0
            self.bdi_msg.manipulate_params.desired.pelvis_yaw = 0.0
            # ----
            self.bdi_msg.behavior = self.MANIPULATE
            self.bdi_msg.k_effort = self.user_msg.k_effort
            self.pub_asic.publish(self.bdi_msg)
            
        elif current_behavior == self.USER:
            self.bdi_msg.behavior = self.STAND
            self.pub_asic.publish(self.bdi_msg)
            
        elif current_behavior == self.FREEZE:
            self.bdi_msg.behavior = self.STAND
            self.pub_asic.publish(self.bdi_msg)
        
        elif current_behavior == self.STAND_PREP:
            self.bdi_msg.behavior = self.STAND
            self.pub_asic.publish(self.bdi_msg)
        
        elif current_behavior == self.WALK:
            self.bdi_msg.behavior = self.STAND
            self.pub_asic.publish(self.bdi_msg)
        
        elif current_behavior == self.STEP:
            self.bdi_msg.behavior = self.STAND
            self.pub_asic.publish(self.bdi_msg)
        
        elif current_behavior == self.MANIPULATE:            
            # Avoid overloading the atlas_command
            self.state = self.STAND
            
if __name__ == '__main__':
    rospy.init_node('control_mode_switching')
    atlas_controller = ControlSwitch()
    rate = 500      # Hz
    rospy.loginfo('Control Mode Switching running at '+str(rate)+' Hz')
    r = rospy.Rate(rate) 
    while not rospy.is_shutdown():
        atlas_controller.update()
        r.sleep()
