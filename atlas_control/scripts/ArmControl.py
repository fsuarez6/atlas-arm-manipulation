#!/usr/bin/env python
#
# Copyright 2013 Open Source Robotics Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Desc: helper script for spawning models in gazebo
#
# Author: Francisco Suárez Ruiz
#

import rospy, time
from math import ceil 
from numpy import array, linspace  
from atlas_msgs.msg import AtlasState
from sensor_msgs.msg import JointState
from osrf_msgs.msg import JointCommands
from sandia_hand_msgs.msg import SimpleGrasp
from sandia_hand_msgs.srv import SimpleGraspSrv
from kinematics_msgs.srv import GetPositionIK
from kinematics_msgs.srv import GetPositionIKRequest
from kinematics_msgs.srv import GetPositionIKResponse
from kinematics_msgs.srv import GetKinematicSolverInfo
from kinematics_msgs.srv import GetKinematicSolverInfoRequest
from kinematics_msgs.srv import GetKinematicSolverInfoResponse
from arm_navigation_msgs.srv import SetPlanningSceneDiff
from arm_navigation_msgs.srv import SetPlanningSceneDiffRequest
from arm_navigation_msgs.srv import SetPlanningSceneDiffResponse

grasp_types = ['cylindrical', 'prismatic', 'spherical']

class ArmControl:      
    def __init__(self, arm_name):
        self._arm_name = arm_name
        self._kinematics_service = 'atlas_'+self._arm_name+'_kinematics'
        if self._arm_name == 'right_arm':
            self._hand_name = 'r_hand'
        elif self._arm_name == 'left_arm':
            self._hand_name = 'l_hand'
        # The name of the root and tip links should be published by the kinematics service
        self._root_link = rospy.get_param(self._kinematics_service + '/manipulator/root_name') 
        self._tip_link = rospy.get_param(self._kinematics_service + '/manipulator/tip_name')
        print 'Using:'
        print '   Kinematics service: ' + self._kinematics_service
        print '   _root_link: %s   _tip_link: %s ' % (self._root_link, self._tip_link)
        # Make sure the inverse kinematics service is running
        self._ik_info_srv_name = self._kinematics_service + '/get_ik_solver_info'
        self._ik_srv_name = self._kinematics_service + '/get_ik'
        self._SET_PLANNING_SCENE_DIFF_NAME = "/environment_server/set_planning_scene_diff"
        rospy.loginfo('Waiting for %s kinematics services' % arm_name)
        rospy.wait_for_service(self._ik_info_srv_name)
        rospy.wait_for_service(self._ik_srv_name)
        # Latest message from atlas/joint_states
        self._jointState_msg = JointState()
        # Set up publishers / subscribers
        rospy.Subscriber('atlas/joint_states', JointState, self.__jointState_cb)
        self._pub_user_mode = rospy.Publisher('atlas/'+self._arm_name+'_control', JointCommands)
        # Gives time to take effect
        self._pub_user_mode.publish(JointCommands())
        # Set the planning scene to null, else IK calls will fail
        try:
            rospy.wait_for_service(self._SET_PLANNING_SCENE_DIFF_NAME, 1.5)
            set_planning_scene_diff_client = rospy.ServiceProxy(self._SET_PLANNING_SCENE_DIFF_NAME,
                                                        SetPlanningSceneDiff)
            planning_scene_req = SetPlanningSceneDiffRequest()
            planning_scene_res = SetPlanningSceneDiffResponse()
            if not set_planning_scene_diff_client.call(planning_scene_req, planning_scene_res):
                rospy.ERROR("Can't set planning scene, IK service calls will fail.")
        except rospy.ROSException, e:
            print 'Planning scene did not process request: '+str(e)  
        # Setup service clients to get kinematic solutions
        self._IK_INFO_SERVICE = rospy.ServiceProxy(self._ik_info_srv_name, GetKinematicSolverInfo)
        self._IK_SERVICE = rospy.ServiceProxy(self._ik_srv_name, GetPositionIK)
        # Define the service messages
        self._info_request = GetKinematicSolverInfoRequest()
        self._info_response = GetKinematicSolverInfoResponse()
         
        # Get the list of joint names from the kinematics solver
        try:
            self._info_response = self._IK_INFO_SERVICE.call(self._info_request)
            self._joint_names = self._info_response.kinematic_solver_info.joint_names
            self._NUM_JOINTS = len(self._joint_names)
            for index in xrange(self._NUM_JOINTS):
                print '[KINEMATICS] Joint %d: %s' % (index, self._joint_names[index])
        except rospy.ServiceException, e:
            rospy.signal_shutdown('Could not call query service')

        # Define the kinematics service messages
        self._gpik_req = GetPositionIKRequest()
        self._gpik_res = GetPositionIKResponse()
        
        # Timeout for finding a solution 
        self._gpik_req.timeout = rospy.Duration(10.0)
        # End effector frame (must be link name of last link or tool point frame)
        self._gpik_req.ik_request.ik_link_name = self._tip_link
        # Reference frame for the desired pose, eg. base_link
        self._gpik_req.ik_request.pose_stamped.header.frame_id = '/pelvis';
        # Creates sandia hands simple grasp client and waits for the server
        rospy.loginfo('Waiting for sandia hand simple grasp service')
        self._grasp_srv_name = '/sandia_hands/%s/simple_grasp' % self._hand_name 
        rospy.wait_for_service(self._grasp_srv_name)
        
    def __jointState_cb(self, msg):
        self._jointState_msg = msg
        if self._arm_name == 'right_arm':
            self._current_joint_position = list(msg.position[AtlasState.r_arm_usy:AtlasState.r_arm_mwx+1])
        elif self._arm_name == 'left_arm':
            self._current_joint_position = list(msg.position[AtlasState.l_arm_usy:AtlasState.l_arm_mwx+1])
    
    # TODO: Calculate the goal time from the joints limits    
    def moveToJointGoal(self, dt, goal_angles): 
        if not (len(goal_angles) == self._NUM_JOINTS):
            print '['+self._arm_name+'] Incorrect number of joints'
            return
        initial_angles = array(self._current_joint_position)
        # Creates the command message that will be sent to '/atlas/'+_arm_name+'_control'
        command = JointCommands()
        command.name = self._joint_names
        command.header.stamp = rospy.Time.now()
        commandPosition = array(goal_angles)
        goal_time = dt
        dtPublish = 0.02
        n = ceil((goal_time) / dtPublish)
        print '['+self._arm_name+'] Moving arm'
        for ratio in linspace(0, 1, n):
            interpCommand = (1-ratio)*initial_angles + ratio * commandPosition
            command.position = [ float(x) for x in interpCommand ]
            self._pub_user_mode.publish(command)
            rospy.sleep(goal_time / float(n))
            
    def moveToPoseGoal(self, dt, position, orientation):
        # Seed the solver with a guess for the joint positions, using the mid-point of the joint limits
        # Note that if using a numerical solver, it should search for solutions using multiple random seeds,
        # or seeds based on the previous known joint positions, otherwise it will often fail.
        self._gpik_req.ik_request.ik_seed_state.joint_state.name = self._joint_names
#         for i in xrange(self._NUM_JOINTS):
#             self._gpik_req.ik_request.ik_seed_state.joint_state.position[i] = (self._info_response.kinematic_solver_info.limits[i].min_position + self._info_response.kinematic_solver_info.limits[i].max_position)/2.0;
        self._gpik_req.ik_request.ik_seed_state.joint_state.position = list(self._current_joint_position)
        #Update IK request with pose
        self._gpik_req.ik_request.pose_stamped.pose.position.x = position[0]
        self._gpik_req.ik_request.pose_stamped.pose.position.y = position[1]
        self._gpik_req.ik_request.pose_stamped.pose.position.z = position[2]
        self._gpik_req.ik_request.pose_stamped.pose.orientation.w = orientation[0]
        self._gpik_req.ik_request.pose_stamped.pose.orientation.x = orientation[1]
        self._gpik_req.ik_request.pose_stamped.pose.orientation.y = orientation[2]
        self._gpik_req.ik_request.pose_stamped.pose.orientation.z = orientation[3]
        try:
            self._gpik_res = self._IK_SERVICE(self._gpik_req)
            if self._gpik_res.error_code.val == self._gpik_res.error_code.SUCCESS:
                print '[KINEMATICS] IK solution found'
                self.moveToJointGoal(dt, list(self._gpik_res.solution.joint_state.position))
            else:
                print '[IK_ERROR] IK solution NOT found'
        except rospy.ServiceException, e:
            print '[IK_SRV_ERROR] Inverse kinematics service call failed: ' + str(e)
            
    def simpleGrasp(self, dt, grasp, amount):
        try:
            sgs = rospy.ServiceProxy(self._grasp_srv_name, SimpleGraspSrv)
            sgs(SimpleGrasp(grasp_types[grasp], amount))
            # Wait for the grasp command to be performed
            time.sleep(dt)
        except rospy.ServiceException, excep:
            print "[GRASP_ERROR] service call failed: %s" % excep
